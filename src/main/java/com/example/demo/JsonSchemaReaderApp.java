package com.example.demo;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transaction;
import java.io.IOException;
import java.util.List;

@SpringBootApplication
public class JsonSchemaReaderApp {
    public static void main(String[] args) {
        SpringApplication.run(JsonSchemaReaderApp.class, args);

    }
}
       /* CsvMapper csvMapper = new CsvMapper();
        CsvSchema csvSchema = csvMapper.typedSchemaFor(Transaction.class).withHeader();
        List list = new CsvMapper().readerFor(Transaction.class)
                .with(csvSchema.withColumnSeparator(CsvSchema.DEFAULT_COLUMN_SEPARATOR))
                .readValues(JsonSchemaReaderApp.class.getClassLoader().getResource("data.csv"))
                .readAll();
        for (int i = 0; i < list.size(); i++) {
            System.out.printf(" Transaction Row [%d] : %s \n", i + 1, list.get(i));
        }
    }
}
*/