package com.example.demo;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.io.InputStream;

public class csvExample {
    private MappingIterator<Transaction> readTruckEventsFromCsv(InputStream csvStream) throws IOException {
        CsvSchema bootstrap = CsvSchema.builder()
// driverId,truckId,eventTime,eventType,longitude,latitude,eventKey,correlationId,driverName,routeId,
// routeName,eventDate
                .addColumn("Id", CsvSchema.ColumnType.STRING)
                .addColumn("amount", CsvSchema.ColumnType.NUMBER)
                .addColumn("currency", CsvSchema.ColumnType.STRING)
                .build().withHeader();

        CsvMapper csvMapper = new CsvMapper();
        return csvMapper.readerFor(Transaction.class).with(bootstrap).readValues(csvStream);
    }
}
