package com.example.demo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;
import java.math.BigDecimal;


@JsonPropertyOrder(value = {"id", "amount", "currency"})
@JsonRootName("transaction")
public class Transaction implements Serializable {
    public enum Currency {
        INR, HKD, EUR, USD
    }
    @JsonProperty
    private String id;
    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private BigDecimal amount;
    @JsonProperty
    private Currency currency;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public double getAmount() {
        return amount.doubleValue();
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Transaction{");
        sb.append("id='").append(id).append('\'');
        sb.append(", amount=").append(amount);
        sb.append(", currency=").append(currency);
        sb.append('}');
        return sb.toString();
    }
}

